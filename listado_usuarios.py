#!/usr/bin/env python
"""Script para mostrar el propietario y los permisos de un DIRECTORIO
   y todos sus subdirectorios"""

import os, pwd, stat

DIR="/home/jose"
def getowner(directory):
    """Detecta el propietario y el grupo de un directorio y lo muestra"""
    nombre=[]
    nombre = pwd.getpwuid(os.stat(directory).st_uid).pw_name, \
            pwd.getpwuid(os.stat(directory).st_gid).pw_name
    print "==================================================="
    print "DIRECTORIO (%s)" % (directory,)
    return "Propietario del directorio %s: %s\n\
Grupo al que pertenece el directorio %s: \
%s" % (directory, nombre[0], directory, nombre[1])
    


def chequeopermisos(directory):
    """Detecta los permisos que tiene un directorio y los muestra"""
    print "==================================================="
    mode=stat.S_IMODE(os.lstat(directory)[stat.ST_MODE])
    print "\nPermisos  para el directorio ", directory, ":"
    for level in "USR", "GRP", "OTH":
        for perm in "R", "W", "X":
            if mode & getattr(stat,"S_I"+perm+level):
                print level, " tiene permiso ", perm
            else:
                print level, " no tiene  permiso ", perm  
    return ""
for root, dirs, files in os.walk(DIR):
    print  getowner(root)
    print  chequeopermisos(root)



